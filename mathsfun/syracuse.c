/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syracuse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/20 16:29:06 by lmunoz            #+#    #+#             */
/*   Updated: 2016/06/05 21:47:00 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// ce programme affiche la suite de Syracuse, attention a pas depasser un int
#include <stdio.h>

int   term(int nb)
{
	if(nb % 2 == 0)
		nb = nb / 2;
	else
		nb = (nb * 3) + 1;
	return(nb);
}

int main ()
{
	printf("Entrez un Nombre pour afin d'afficher la suite de Syracuse\n");
	int nombr;
	scanf("%d", &nombr);
	while(nombr != 1)
	{
		nombr = term(nombr);
		printf("%d ", nombr);
	}
	return(0);
}
