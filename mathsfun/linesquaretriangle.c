/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linesquaretriangle.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 17:54:24 by lmunoz            #+#    #+#             */
/*   Updated: 2016/05/30 15:22:08 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
// affiche une ligne, un carre et un triangle.  LVL EASY

#include <stdio.h>

void	printline(int colonnes, char motif)
{
	int i = 0;
	while(i++ < colonnes)
		printf("%c", motif);
	printf("\n");
}

void	printmiddle(int colonnes, char motif, char middle)
{
	int i = 0;
	while(i < colonnes)
	{
		if(i == 0 || i == (colonnes - 1))
			printf("%c", motif);
		else
			printf("%c", middle);
		i++;
	}
	printf("\n");
}

void	printsquare(int lignes, int colonnes, char motif)
{
	int i = 0;

	while(i < lignes)
	{
		if(i == 0 || i == (lignes - 1))
			printline(colonnes, motif);
		else
			printmiddle(colonnes, motif, ' ');
		i++;
	}
}

void	printtriangle(int lignes, char motif)
{
	int i = 0;

	while(i < lignes)
	{
		int colonnes = i + 1;
		if(i == 0 || i == (lignes - 1))
			printline(colonnes, motif);
		else
			printmiddle(colonnes, motif, ' ');
		i++;
	}
}

int main()
{
	int lignes, colonnes;

	printf("Combien voulez vous de caracteres a afficher : ?\n");
	scanf("%d", &colonnes);
	printline(colonnes, 'X');

	printf("Afficher un carre de combien de lignes et de colonnes : ?\n");
	scanf("%d %d", &lignes, &colonnes);
	printsquare(lignes, colonnes, '#');

	printf("Afficher un triangle rectangle de comgien de lignes : ?\n");
	scanf("%d", &lignes);
	printtriangle(lignes, '@');
}
